package com.healthitda.android.utils;

import com.squareup.otto.Bus;

/**
 * Created by mouli on 4/28/15.
 */
public class EventBus {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

}
