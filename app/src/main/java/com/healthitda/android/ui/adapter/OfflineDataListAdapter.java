package com.healthitda.android.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.healthitda.android.R;
import com.healthitda.android.model.OfflineData;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by mouli on 5/21/15.
 */
public class OfflineDataListAdapter extends BaseAdapter {
    private static final String TAG = "OfflineDataListAdapter.class";

    private List<OfflineData> mInputList;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private Picasso mPicassoCLient;

    public OfflineDataListAdapter(List<OfflineData> mInputList, Context mContext) {
        this.mInputList = mInputList;
        this.mContext = mContext;
        this.mLayoutInflater = LayoutInflater.from(mContext);
        this.mPicassoCLient = Picasso.with(mContext);
    }

    @Override
    public int getCount() {
        return mInputList.size();
    }

    @Override
    public OfflineData getItem(int position) {
        return mInputList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null) {
            convertView = mLayoutInflater.inflate(R.layout.list_item_offline_data, parent, false);
            holder = new ViewHolder();
            holder.tvVillageName = (TextView) convertView.findViewById(R.id.tvVillageName);
            holder.ivChildrenImage = (ImageView) convertView.findViewById(R.id.ivChildrenImage);
            holder.ivANCImage= (ImageView) convertView.findViewById(R.id.ivANCImage);
            holder.ivEmergencyImage = (ImageView) convertView.findViewById(R.id.ivEmergencyImage);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        OfflineData item = getItem(position);
        holder.tvVillageName.setText(String.valueOf(position+1) + ". " + item.getVillage());
        if(!item.getChildrenImage().equals("")) {
            mPicassoCLient.load(item.getChildrenImage())
                    .fit()
                    .into(holder.ivChildrenImage);
        }
        if(!item.getAncImage().equals("")) {
            mPicassoCLient.load(item.getAncImage())
                    .fit()
                    .into(holder.ivANCImage);
        }
        if(!item.getEmergencyImage().equals("")) {
            mPicassoCLient.load(item.getEmergencyImage())
                    .fit()
                    .into(holder.ivEmergencyImage);
        }
        return convertView;
    }

    private class ViewHolder {
        TextView tvVillageName;
        ImageView ivChildrenImage;
        ImageView ivANCImage;
        ImageView ivEmergencyImage;
    }
}
