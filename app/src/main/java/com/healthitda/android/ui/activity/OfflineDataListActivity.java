package com.healthitda.android.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.healthitda.android.R;
import com.healthitda.android.model.OfflineData;
import com.healthitda.android.ui.adapter.OfflineDataListAdapter;
import com.healthitda.android.utils.OfflineDbHelper;

import org.json.JSONException;

import java.util.List;

/**
 * Created by mouli on 5/23/15.
 */
public class OfflineDataListActivity extends BaseActivity {
    private static final String TAG = "OfflineDataActivity";
    public static final String INTENT_KEY_SELECTED_OFFLINE_DATA = "intent_key_selected_offline_data";
    private ListView mListView;
    private List<OfflineData> mInputList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_data_list);
        mListView = (ListView) findViewById(R.id.lvOfflineData);
        initListView();
    }

    private void initListView() {
        OfflineDbHelper offlineDbHelper = new OfflineDbHelper(OfflineDataListActivity.this);
        try {
            mInputList = offlineDbHelper.getAllItems();
            OfflineDataListAdapter adapter = new OfflineDataListAdapter(mInputList, OfflineDataListActivity.this);
            mListView.setAdapter(adapter);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(OfflineDataListActivity.this, OfflineDataActivity.class);
                    intent.putExtra(INTENT_KEY_SELECTED_OFFLINE_DATA, position);
                    startActivity(intent);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

	@Override
	public void onBackPressed() {
		startActivity(new Intent(OfflineDataListActivity.this, SplashActivity.class));
	}
}
