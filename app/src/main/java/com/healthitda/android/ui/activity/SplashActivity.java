package com.healthitda.android.ui.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.healthitda.android.MainActivity;
import com.healthitda.android.R;
import com.healthitda.android.model.Mandal;
import com.healthitda.android.model.OfflineData;
import com.healthitda.android.utils.DbHelper;
import com.healthitda.android.utils.NetworkUtil;
import com.healthitda.android.utils.OfflineDbHelper;
import com.healthitda.android.utils.PrefUtils;
import com.healthitda.android.utils.URLs;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import net.steamcrafted.loadtoast.LoadToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by mouli on 4/28/15.
 */
public class SplashActivity extends BaseActivity {
    private static final String TAG = "SplashActivity.class";
    private static final String PREF_KEY_OFFLINE_TABLE_EXISTS = "pref_key_offline_table_exists";
    private static final String PREF_KEY_IS_DATA_STORED = "pref_key_is_data_stored";
    private static final int SPLASH_TIME_OUT = 2000;
    private LoadToast mLoadToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mLoadToast = new LoadToast(SplashActivity.this);
        mLoadToast.setText("Getting Data...");
        makeOfflineDataTable();
        OfflineDbHelper offlineDbHelper = new OfflineDbHelper(SplashActivity.this);
        int itemCount = offlineDbHelper.getCount();
        if(itemCount!=0) {
            AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this)
                    .setMessage("Do you want to submit stored offline data?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(SplashActivity.this, OfflineDataListActivity.class));
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            proceedWithOnlineProcess();
                        }
                    })
                    .create();
            alertDialog.show();
        } else {
            boolean isDataStored = PrefUtils.getSharedPreferences(SplashActivity.this)
                    .getBoolean(PREF_KEY_IS_DATA_STORED, false);
            Log.d(TAG, "is data stored " + isDataStored);
            if(isDataStored) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    }
                }, 2000);
            } else {
                new ReadFromAssetTask().execute();
            }
//            proceedWithOnlineProcess();
        }

    }

    private void proceedWithOnlineProcess() {
        DbHelper dbHelper = new DbHelper(getBaseContext());
        if(NetworkUtil.haveNetworkConnection(SplashActivity.this)) {
            dbHelper.clearItems();
            new GetSubCenters().execute();
        } else {
            if(dbHelper.getCount()==0) {
                Toast.makeText(SplashActivity.this, "No internet connection and no data found",
                        Toast.LENGTH_SHORT).show();
            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    }
                }, SPLASH_TIME_OUT);
            }
        }
    }

    private void makeOfflineDataTable() {
        boolean tableExsits = PrefUtils.getSharedPreferences(SplashActivity.this).
                getBoolean(PREF_KEY_OFFLINE_TABLE_EXISTS, false);
        if(!tableExsits) {
            OfflineDbHelper offlineDbHelper = new OfflineDbHelper(SplashActivity.this);
            offlineDbHelper.clearItems();
            PrefUtils.getSharedPreferences(SplashActivity.this).edit()
                    .putBoolean(PREF_KEY_OFFLINE_TABLE_EXISTS, true).apply();
        }
    }

    public class GetSubCenters extends AsyncTask<Void,Void,String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if(mLoadToast!=null) {
                mLoadToast.show();
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "GetSubCenters called");
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(URLs.SUB_CENTERS_SERVICE)
                    .get()
                    .build();

            Response response = null;
            String responseBody = "";
            try {
                response = client.newCall(request).execute();
                responseBody = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(response==null) {
                return "";
            }
            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            if(mLoadToast!=null) {
                mLoadToast.success();
            }
            DbHelper dbHelper = new DbHelper(getBaseContext());
            dbHelper.clearItems();
            try {
                JSONArray mainJarr = new JSONArray(response);
                for(int i=0; i<mainJarr.length(); i++) {
                    Mandal mandal = new Mandal();
                    JSONObject currJObj = mainJarr.getJSONObject(i);
                    mandal.setName(currJObj.getString("mandal"));
                    mandal.setPhc(currJObj.getJSONArray("phc"));
                    dbHelper.addItem(mandal);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Server response was not a JsonArray!!");
            }

            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        }
    }

    private class ReadFromAssetTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            try {
                readFromAsset();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mLoadToast.success();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }
            }, 1000);
        }
    }

    private void readFromAsset() throws IOException, JSONException {
        StringBuilder buf=new StringBuilder();
        InputStream json=getAssets().open("mandal_data");
        BufferedReader in=
                new BufferedReader(new InputStreamReader(json, "UTF-8"));
        String str;
        while ((str=in.readLine()) != null) {
            buf.append(str);
        }
        in.close();
        DbHelper dbHelper = new DbHelper(SplashActivity.this);
        dbHelper.clearItems();
        JSONArray mainArray = new JSONArray(buf.toString());
        for(int i=0; i<mainArray.length(); i++) {
            JSONObject jsonObject = mainArray.getJSONObject(i);
            Mandal mandal = new Mandal();
            mandal.setName(jsonObject.getString("mandal"));
            JSONArray phcArray = jsonObject.getJSONArray("phc");
            mandal.setPhc(phcArray);
            Log.d(TAG, "mandal " + mandal);
            Log.d(TAG, "phcs " + phcArray);
            dbHelper.addItem(mandal);
        }
        SharedPreferences.Editor editor = PrefUtils.getSharedPreferences(SplashActivity.this).edit();
        editor.putBoolean(PREF_KEY_IS_DATA_STORED, true);
        editor.apply();
    }
}
