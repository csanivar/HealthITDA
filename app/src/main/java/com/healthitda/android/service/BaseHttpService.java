package com.healthitda.android.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.healthitda.android.model.Mandal;
import com.healthitda.android.utils.DbHelper;
import com.healthitda.android.utils.URLs;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by mouli on 4/28/15.
 */
public class BaseHttpService extends Service {
    private static final String TAG = "BaseHttpService.class";
    private Context mContext;

    private final IBinder mBinder = new HttpGetBinder();

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    public class HttpGetBinder extends Binder {
        public BaseHttpService getService() {
            // Return this instance of BaseHttpGetService so clients can call public methods
            return BaseHttpService.this;
        }
    }

    public BaseHttpService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        /*CustomRequest request = null;
        if (intent != null) {
            if (intent.hasExtra(REQUEST_ID)) {
                // retrieving the intent data
                int requestId = intent.getIntExtra(REQUEST_ID, 0);
                String url = intent.getStringExtra(URL);
                int requestType = intent.getIntExtra(REQUEST_TYPE, DEFAULT_REQUEST_TYPE);
                String[] params = intent.getStringArrayExtra(PARAMS);

                // check to see if data is valid and proceed
                if (requestId != 0) {
                    request = new CustomRequest(requestId, url, params, requestType);
                    newRequest(request);
                } else {
                    LogUtils.LOGE(TAG, "The intent to BaseHttpService is malformed");
                    LogUtils.LOGE(TAG, "Received: requestId = " + requestId);
                    LogUtils.LOGE(TAG, "Received: url = " + url);
                    LogUtils.LOGE(TAG, "Received: requestType = " + requestType);
                }
            } else if (intent.hasExtra(REQUEST_PARCELABLE)) {
                request = intent.getParcelableExtra(REQUEST_PARCELABLE);
                newRequest(request);
            }
        }

        LogUtils.LOGD(TAG, "onStartCommand called with request: " + request);*/
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind called");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy called");
    }
/*
    *//**
     * adds a new request either to queue (as sync request)
     * or executes as async request depending on requestType
     *
     * @param request the request to be added
    public void newRequest(CustomRequest request) {
    if (request.getRequestType() == SYNC_REQUEST) {
    newSyncRequest(request);
    } else {
    newAsyncRequest(request);
    }
    }

     *//**//**
     * adds a new request and adds the callback to the hashmap
     * @param request the request to be added
     * @param callback the callback to be called after execution of request
     *//**//*
    public void newRequest(CustomRequest request, RequestCallback callback) {
        mCallbacksMap.put(request, callback);
        newRequest(request);
    }

    *//**//**
     * AsyncTask that handles the requests
     *//**//*
    private class HttpRequestTask extends AsyncTask<String, Void, Void> {
        private HttpResult httpResult;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            EventBus.getDefault().post(new NormalEvents.SyncEvent(true));
        }

        @Override
        protected CustomRequest doInBackground(CustomRequest... params) {
            CustomRequest customRequest = params[0];
            QueryResolver.preExecuteQuery(mContext, customRequest);
            publishProgress(customRequest);
            httpResult = RequestResolver.getByRequestId(customRequest, mContext);
            customRequest.setSuccess(!httpResult.isError);
            QueryResolver.postExecuteQuery(mContext, customRequest, !httpResult.isError);
            return customRequest;
        }

        protected void onProgressUpdate(CustomRequest... progress) {
            setRequestStatus(progress[0], REQUEST_STATUS_QUERYING);
        }

        protected void onPostExecute(CustomRequest request) {
            startNextSyncRequest();
            if (request != null) {
                startCallback(request);
                removeRequest(request);
            }
            if(httpResultListener != null) {
                httpResultListener.onHttpResult(httpResult);
            }else{
                isListenerActive = false;
            }
            checkForTermination();
            stopSync();
        }
    }*/

    public void getSubCenters() {
        new GetSubCenters().execute();
    }

    public class GetSubCenters extends AsyncTask<Void,Void,String> {

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "GetSubCenters called");
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(URLs.SUB_CENTERS_SERVICE)
                    .build();

            Response response = null;
            try {
                response = client.newCall(request).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(response==null) {
                return "";
            }
            return response.toString();
        }

        @Override
        protected void onPostExecute(String response) {
            DbHelper dbHelper = new DbHelper(mContext);
            dbHelper.clearItems();
            try {
                JSONArray mainJarr = new JSONArray(response);
                for(int i=0; i<mainJarr.length(); i++) {
                    Mandal mandal = new Mandal();
                    JSONObject currJObj = mainJarr.getJSONObject(i);
                    mandal.setName(currJObj.getString("mandal"));
                    mandal.setPhc(currJObj.getJSONArray("phc"));
                    dbHelper.addItem(mandal);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Server response was not a JsonArray!!");
            }
        }
    }
}
