package com.healthitda.android;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.healthitda.android.model.Mandal;
import com.healthitda.android.ui.activity.BaseActivity;
import com.healthitda.android.ui.activity.ChildrenActivity;
import com.healthitda.android.ui.activity.OfflineDataListActivity;
import com.healthitda.android.utils.DbHelper;
import com.healthitda.android.utils.NetworkUtil;
import com.healthitda.android.utils.OfflineDbHelper;
import com.healthitda.android.utils.URLs;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends BaseActivity {
    private static final String TAG  = "MainActivity.class";
    public static final String INTENT_KEY_STORE_OFFLINE = "store_offline";
    TextView tvMandalName;
    TextView tvMandalPHC;
    TextView tvSubCenter;
    TextView tvVillage;
    EditText etName;
    Button btnNext;
    String mandal;
    String phc;
    String subCenter;
    String village;
    ImageView ivLogo;
    List<Mandal> mandalsList;
    int selectedMandalPosition = -1;
    int selectedSubCenterPosition = -1;
    int selectedVillagesPosition = -1;
    int selectedPHCPosition = -1;
    List<String> mandalNamesList;
    List<String> subCentersList;
    List<String> phcList;
    List<String> villagesList;
    JSONArray phcJSONArray;
    JSONArray subCentersJSONArray;
    ProgressDialog mProgressDialog;
    ProgressDialog mProgressDialog2;
    private boolean storeOffline = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        if(NetworkUtil.haveNetworkConnection(MainActivity.this)) {
            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                    .setMessage("Do you want to save data offline?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            storeOffline = true;
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            storeOffline = false;
                        }
                    })
                    .create();
            alertDialog.show();
        } else {
            storeOffline = true;
        }
        try {
            init();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void init() throws JSONException {
        tvMandalName = (TextView) findViewById(R.id.tvMandal);
        tvMandalPHC = (TextView) findViewById(R.id.tvPHC);
        tvSubCenter = (TextView) findViewById(R.id.tvSubCenter);
        tvVillage = (TextView) findViewById(R.id.tvVillage);
        etName = (EditText) findViewById(R.id.etName);
        ivLogo = (ImageView) findViewById(R.id.ivLogo);
        btnNext = (Button) findViewById(R.id.btnNext);

        OfflineDbHelper offlineDbHelper = new OfflineDbHelper(MainActivity.this);
        if(offlineDbHelper.getCount()!=0) {
            findViewById(R.id.tvOfflineHint).setVisibility(View.VISIBLE);
            ivLogo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, OfflineDataListActivity.class));
                }
            });
        }

        DbHelper dbHelper = new DbHelper(this);
        Log.d(TAG, "db count " + dbHelper.getCount());
        if(dbHelper.getCount()==0) {
            new GetSubCenters().execute();
        } else {
            mandalsList  = dbHelper.getAllItems();
        }
        tvMandalName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectMandalDialog(MainActivity.this);
            }
        });
        tvMandalPHC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mandal==null) {
                    Toast.makeText(MainActivity.this, "Please select a mandal", Toast.LENGTH_SHORT).show();
                }
                try {
                    selectPHCDialog(MainActivity.this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        tvSubCenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phc == null) {
                    Toast.makeText(MainActivity.this, "Please select a phc", Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    selectSebCenterDialog(MainActivity.this);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        tvVillage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(subCenter==null) {
                    Toast.makeText(MainActivity.this, "Please select a sub center", Toast.LENGTH_LONG).show();
                    return;
                }
                selectVillageDialog(MainActivity.this);
            }
            });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mandal == null ||
                        phc == null ||
                        subCenter == null ||
                        village == null ||
                        etName.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Please fill all values before proceeding", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(MainActivity.this, ChildrenActivity.class);
                intent.putExtra(ChildrenActivity.INTENT_KEY_MANDAL, mandal);
                intent.putExtra(ChildrenActivity.INTENT_KEY_PHC, phc);
                intent.putExtra(ChildrenActivity.INTENT_KEY_SUB_CENTER, subCenter);
                intent.putExtra(ChildrenActivity.INTENT_KEY_VILLAGE, village);
                intent.putExtra(ChildrenActivity.INTENT_KEY_NAME, etName.getText().toString());
                intent.putExtra(INTENT_KEY_STORE_OFFLINE, storeOffline);
                startActivity(intent);
            }
        });
    }

    private void selectMandalDialog(Context context) {
        final Dialog dialog  = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_list_dialog);
        ListView lvItems = (ListView) dialog.findViewById(R.id.lvItems);
        mandalNamesList = new ArrayList<>();
        for(int i=0; i<mandalsList.size(); i++) {
            mandalNamesList.add(mandalsList.get(i).getName());
        }
        ListAdapter adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, mandalNamesList);
        if(lvItems!=null) {
            lvItems.setAdapter(adapter);
        } else {
            Log.d(TAG, "lvItems is null");
        }
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mandal = mandalNamesList.get(position);
                tvMandalName.setText(mandal);
                phcList = new ArrayList<String>();
                phcJSONArray = mandalsList.get(position).getPhc();
                for(int i=0;i<phcJSONArray.length();i++) {
                    try {
                        phcList.add(phcJSONArray.getJSONObject(i).getString("phc"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                tvMandalPHC.setText(phc);
                selectedMandalPosition = position;
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void selectPHCDialog(Context context) throws JSONException {
        final Dialog dialog  = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_list_dialog);
        ListView lvItems = (ListView) dialog.findViewById(R.id.lvItems);
        ListAdapter adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, phcList);
        if(lvItems!=null) {
            lvItems.setAdapter(adapter);
        } else {
            Log.d(TAG, "lvItems is null");
        }
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                phc = phcList.get(position);
                tvMandalPHC.setText(phc);
                selectedPHCPosition = position;
                subCentersList = new ArrayList<String>();
                try {
                    subCentersJSONArray = phcJSONArray.getJSONObject(position).getJSONArray("subcenter");
                    for(int i=0; i<subCentersJSONArray.length(); i++) {
                        subCentersList.add(subCentersJSONArray.getJSONObject(i).getString("subcenter"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void selectSebCenterDialog(Context context) throws JSONException {
        final Dialog dialog  = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_list_dialog);
        ListView lvItems = (ListView) dialog.findViewById(R.id.lvItems);
        /*subCentersList = new ArrayList<>();
        JSONArray subCentersJArray = mandalsList.get(selectedMandalPosition).getSubCenters();
        for(int i=0; i<subCentersJArray.length(); i++) {
            String subCenter = subCentersJArray.getJSONObject(i).getString("subcenter");
            subCentersList.add(subCenter);
        }*/
        ListAdapter adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, subCentersList);
        if(lvItems!=null) {
            lvItems.setAdapter(adapter);
        } else {
            Log.d(TAG, "lvItems is null");
        }
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                subCenter = subCentersList.get(position);
                tvSubCenter.setText(subCenter);
                selectedSubCenterPosition = position;
                villagesList = new ArrayList<String>();
                try {
                    JSONArray villagesJSONArray = subCentersJSONArray.getJSONObject(position).getJSONArray("villages");
                    for(int i=0; i<villagesJSONArray.length(); i++) {
                        villagesList.add(villagesJSONArray.getString(i));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

//                new GetVillages().execute();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void selectVillageDialog(Context context) {
        final Dialog dialog  = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_list_dialog);
        ListView lvItems = (ListView) dialog.findViewById(R.id.lvItems);
        ListAdapter adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, villagesList);
        if(lvItems!=null) {
            if(adapter!=null) {
                lvItems.setAdapter(adapter);
            } else {
                Log.e(TAG, "village list adapter is null");
            }
        } else {
            Log.d(TAG, "lvItems is null");
        }
        lvItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                village = villagesList.get(position);
                selectedVillagesPosition = position;
                tvVillage.setText(village);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public class GetSubCenters extends AsyncTask<Void,Void,String> {

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setMessage("Getting Data...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "GetSubCenters called");
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(URLs.SUB_CENTERS_SERVICE)
                    .build();

            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
                Log.d(TAG, "Response " + response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }
            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            mProgressDialog.dismiss();
            Log.d(TAG, "response from on post execute" + response);
            DbHelper dbHelper = new DbHelper(getBaseContext());
            dbHelper.clearItems();
            try {
                JSONArray mainJarr = new JSONArray(response);
                for(int i=0; i<mainJarr.length(); i++) {
                    Mandal mandal = new Mandal();
                    JSONObject currJObj = mainJarr.getJSONObject(i);
                    mandal.setName(currJObj.getString("mandal"));
                    mandal.setPhc(currJObj.getJSONArray("phc"));
                    dbHelper.addItem(mandal);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Server response was not a JsonArray!!");
            }
            Log.d(TAG, "db count " + dbHelper.getCount());
        }
    }

    public class GetVillages extends AsyncTask<Void,Void,String> {

        @Override
        protected void onPreExecute() {
            mProgressDialog2 = new ProgressDialog(MainActivity.this);
            mProgressDialog2.setMessage("Getting Data...");
            mProgressDialog2.setCancelable(false);
            mProgressDialog2.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "GetVillages called");
            OkHttpClient client = new OkHttpClient();
            RequestBody requestBody = new FormEncodingBuilder()
                    .add("subcenter" , subCenter)
                    .build();
            String encodedUrl = null;
            try {
                encodedUrl = URLEncoder.encode(URLs.VILLAGES_SERVICE
                        + subCentersList.get(selectedSubCenterPosition), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            /*URL url = null;
            try {
                url = new URL(URLs.VILLAGES_SERVICE+subCentersList.get(selectedSubCenterPosition));
            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.e(TAG, "malformed url");
            }*/
            Request request = new Request.Builder()
                    .url(URLs.VILLAGES_SERVICE)
                    .post(requestBody)
                    .build();

            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
                Log.d(TAG, "Response " + response.body().string());
            } catch (IOException e) {tvVillage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(subCenter==null) {
                    Toast.makeText(MainActivity.this, "Please select a sub center", Toast.LENGTH_LONG).show();
                    return;
                }
                selectVillageDialog(MainActivity.this);
            }
        });
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }
            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            mProgressDialog2.dismiss();
            Log.d(TAG, "response from on post execute" + response);
            villagesList = new ArrayList<>();
            try {
                JSONArray mainJarr = new JSONArray(response);
                for(int i=0; i<mainJarr.length(); i++) {
                    String villageName = mainJarr.getJSONObject(i).getString("village");
                    villagesList.add(villageName);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "Server response was not a JsonArray!!");
            }
        }
    }
}
