package com.healthitda.android.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.healthitda.android.MainActivity;
import com.healthitda.android.R;
import com.healthitda.android.model.OfflineData;
import com.healthitda.android.ui.adapter.OfflineDataListAdapter;
import com.healthitda.android.utils.OfflineDbHelper;
import com.healthitda.android.utils.PrefUtils;
import com.healthitda.android.utils.URLs;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import net.steamcrafted.loadtoast.LoadToast;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.prefs.PreferenceChangeEvent;

/**
 * Created by mouli on 5/21/15.
 */
public class OfflineDataActivity extends Activity {
    private static final String TAG = "OfflineDataActivity";
    TextView tvMandal;
    TextView tvPHC;
    TextView tvSubCenter;
    TextView tvVillage;
    TextView tvName;
    TextView tvChildrenNum;
    TextView tvChildrenTotal;
    ImageView ivChildrenImage;
    TextView tvAncNum;
    TextView tvAncImmunised;
    TextView tvAncMctsNames;
    ImageView ivANCImage;
    TextView tvEmergencyNum;
    TextView tvEmergencyReasons;
    ImageView ivEmergencyImage;
    Button btnSubmit;
    LoadToast mLoadToast;
    OfflineData mItem;
    String mSuccessId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline);
        mLoadToast = new LoadToast(OfflineDataActivity.this);
        initViews();
        initItem();

    }

    private void initItem() {
        int position = getIntent().getIntExtra(OfflineDataListActivity.INTENT_KEY_SELECTED_OFFLINE_DATA, -1);
        if(position==-1) {
            Log.d(TAG, "Invalid data");
            return;
        }
        try {
            List<OfflineData> mItems = new OfflineDbHelper(OfflineDataActivity.this).getAllItems();
            mItem = mItems.get(position);
            setView(mItem);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initViews() {
        tvMandal = (TextView) findViewById(R.id.tvMandal);
        tvPHC = (TextView) findViewById(R.id.tvPHC);
        tvSubCenter = (TextView) findViewById(R.id.tvSubCenter);
        tvVillage = (TextView) findViewById(R.id.tvVillage);
        tvName = (TextView) findViewById(R.id.tvName);
        tvChildrenNum = (TextView) findViewById(R.id.tvChildrenNum);
        tvChildrenTotal = (TextView) findViewById(R.id.tvChildrenTotal);
        tvAncNum = (TextView) findViewById(R.id.tvAncNum);
        tvAncImmunised = (TextView) findViewById(R.id.tvANCImmunised);
        tvAncMctsNames = (TextView) findViewById(R.id.tvANCNameMcts);
        tvEmergencyNum = (TextView) findViewById(R.id.tvEmergencyNum);
        tvEmergencyReasons = (TextView) findViewById(R.id.tvEmergencyReasons);
        ivChildrenImage = (ImageView) findViewById(R.id.ivChildrenImage);
        ivANCImage = (ImageView) findViewById(R.id.ivANCImage);
        ivEmergencyImage = (ImageView) findViewById(R.id.ivEmergencyImage);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SubmitChildrenData().execute();
            }
        });
    }

    private void setView(OfflineData item) {
        tvMandal.setText(item.getMandal());
        tvPHC.setText(item.getPhc());
        tvSubCenter.setText(item.getSubCenter());
        tvVillage.setText(item.getVillage());
        tvName.setText(item.getName());
        tvChildrenTotal.setText(item.getChildrenTotal());
        tvChildrenNum.setText(item.getChildrenNum());
        tvAncNum.setText(item.getAncNum());
        tvAncImmunised.setText(item.getAncNumImmnised());
        tvAncMctsNames.setText(item.getAncNameMcts());
        tvEmergencyNum.setText(item.getEmergencyNum());
        tvEmergencyReasons.setText(item.getEmergencyReasons());
        Log.d(TAG, "children image name " + item.getChildrenImage());
        if(!item.getChildrenImage().equals("")) {
            Picasso.with(OfflineDataActivity.this).load(Uri.parse(item.getChildrenImage()))
                    .fit()
                    .into(ivChildrenImage);
        }
        if(!item.getAncImage().equals("")) {
            Picasso.with(OfflineDataActivity.this).load(Uri.parse(item.getAncImage()))
                    .fit()
                    .into(ivANCImage);
        }
        if(!item.getEmergencyImage().equals("")) {
            Picasso.with(OfflineDataActivity.this).load(Uri.parse(item.getEmergencyImage()))
                    .fit()
                    .into(ivEmergencyImage);
        }
    }

    public class SubmitChildrenData extends AsyncTask<Void,Void,String> {

        @Override
        protected void onPreExecute() {
            mLoadToast.setText("Uploading data...");
            mLoadToast.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            OkHttpClient client = new OkHttpClient();
            final File file = new File(getRealPathFromURI(Uri.parse(mItem.getChildrenImage()), OfflineDataActivity.this));

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addPart(Headers.of("Content-Disposition", "form-data; name=\"mandal\""),
                            RequestBody.create(null, mItem.getMandal()))
                    .addFormDataPart("phc", mItem.getPhc())
                    .addFormDataPart("subcenter", mItem.getSubCenter())
                    .addFormDataPart("village", mItem.getVillage())
                    .addFormDataPart("name", mItem.getName())
                    .addFormDataPart("childern_num", mItem.getChildrenNum())
                    .addFormDataPart("childern_total", mItem.getChildrenTotal())
                    .addPart(Headers.of("Content-Disposition", "form-data;name=\"fileToUpload\"; filename=\"" + file.getName() + "\""),
                            RequestBody.create(null, file)
                    )
                    .build();

            Request request = new Request.Builder()
                    .url(URLs.SAVE_DATA_SERVICE)
                    .removeHeader("Content-Type")
                    .post(requestBody)
                    .build();
            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }
            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            mSuccessId = response.split(":")[1];
            Log.d(TAG, "success id " + mSuccessId);
            new SubmitANCData().execute();
        }
    }

    public class SubmitANCData extends AsyncTask<Void,Void,String> {

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "SubmitANCData called");
            OkHttpClient client = new OkHttpClient();
            final File file = new File(ChildrenActivity.getRealPathFromURI(Uri.parse(mItem.getAncImage()), OfflineDataActivity.this));
            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart("id", String.valueOf(mSuccessId))
                    .addFormDataPart("anc_num", mItem.getAncNum())
                    .addFormDataPart("anc_num_immunised", mItem.getAncNumImmnised())
                    .addFormDataPart("anc_name_mcts", mItem.getAncNameMcts())
                    .addPart(Headers.of("Content-Disposition", "form-data;name=\"anc_pic\"; filename=\"" + file.getName() + "\""),
                            RequestBody.create(null, file)
                    )
                    .build();

            Request request = new Request.Builder()
                    .url(URLs.SAVE_DATA_SERVICE_2)
                    .post(requestBody)
                    .build();

            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
                Log.d(TAG, "Response " + response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }

            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d(TAG, "response from on post execute " + response);
            new SubmitEmergencyData().execute();
        }
    }

    public class SubmitEmergencyData extends AsyncTask<Void,Void,String> {

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "SubmitEmergencyData called");
            OkHttpClient client = new OkHttpClient();
            final File file = new File(getRealPathFromURI(Uri.parse(mItem.getEmergencyImage()), OfflineDataActivity.this));
            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart("id", mSuccessId)
                    .addFormDataPart("emergency_num", mItem.getEmergencyNum())
                    .addFormDataPart("reasons", mItem.getEmergencyReasons())
                    .addPart(Headers.of("Content-Disposition", "form-data;name=\"pic\"; filename=\"" + file.getName() + "\""),
                            RequestBody.create(null, file)
                    )
                    .build();

            Request request = new Request.Builder()
                    .url(URLs.SAVE_DATA_SERVICE_3)
                    .post(requestBody)
                    .build();

            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
                Log.d(TAG, "Response " + response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }

            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            mLoadToast.success();
            SharedPreferences.Editor editor = PrefUtils.getSharedPreferences(OfflineDataActivity.this).edit();
            editor.putBoolean(PrefUtils.PREF_KEY_IS_THERE_OFFLINE_DATA, false) ;
            editor.apply();
            Log.d(TAG, "response from on post execute emergency " + response);
            OfflineDbHelper helper = new OfflineDbHelper(OfflineDataActivity.this);
            helper.deleteItem(mItem);
            Intent intent = new Intent(OfflineDataActivity.this, ThankYouActivity.class);
            startActivity(intent);
        }
    }

    public static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
}
