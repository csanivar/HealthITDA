package com.healthitda.android.ui.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.healthitda.android.MainActivity;
import com.healthitda.android.R;
import com.healthitda.android.model.OfflineData;
import com.healthitda.android.utils.NetworkUtil;
import com.healthitda.android.utils.OfflineDbHelper;
import com.healthitda.android.utils.PrefUtils;

import net.steamcrafted.loadtoast.LoadToast;

/**
 * Created by mouli on 4/28/15.
 */
public class ThankYouActivity extends BaseActivity {
    private static final String TAG = "ThankYouActivity.class";
    private boolean isNetworkAvailable;
    private boolean storeOffline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thank_you);
        isNetworkAvailable = getIntent().getBooleanExtra(NetworkUtil.INTENT_KEY_NO_INTERNET, true);
        storeOffline = getIntent().getBooleanExtra(MainActivity.INTENT_KEY_STORE_OFFLINE, false);
        Log.d(TAG, "isNetworkAvailable " + isNetworkAvailable);
        Log.d(TAG, "storeOffline " + storeOffline);
        if(!isNetworkAvailable || storeOffline) {
            OfflineData item = new OfflineData();
            SharedPreferences sharedPreferences = PrefUtils.getSharedPreferences(ThankYouActivity.this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(PrefUtils.PREF_KEY_IS_THERE_OFFLINE_DATA, true);
            editor.apply();
            item.setMandal(sharedPreferences.getString(PrefUtils.PREF_KEY_MANDAL, ""));
            item.setPhc(sharedPreferences.getString(PrefUtils.PREF_KEY_PHC, ""));
            item.setSubCenter(sharedPreferences.getString(PrefUtils.PREF_KEY_SUB_CENTER, ""));
            item.setVillage(sharedPreferences.getString(PrefUtils.PREF_KEY_VILLAGE, ""));
            item.setName(sharedPreferences.getString(PrefUtils.PREF_KEY_NAME, ""));
            item.setChildrenNum(sharedPreferences.getString(PrefUtils.PREF_KEY_CHILDREN_NUM, ""));
            item.setChildrenTotal(sharedPreferences.getString(PrefUtils.PREF_KEY_CHILDREN_TOTAL, ""));
            item.setChildrenImage(sharedPreferences.getString(PrefUtils.PREF_KEY_CHILDREN_IMAGE, ""));
            item.setAncNum(sharedPreferences.getString(PrefUtils.PREF_KEY_ANC_NUM, ""));
            item.setAncNumImmnised(sharedPreferences.getString(PrefUtils.PREF_KEY_ANC_NUM_IMMNISED, ""));
            item.setAncNameMcts(sharedPreferences.getString(PrefUtils.PREF_KEY_ANC_NAME_MCTS, ""));
            item.setAncImage(sharedPreferences.getString(PrefUtils.PREF_KEY_ANC_IMAGE, ""));
            item.setEmergencyNum(sharedPreferences.getString(PrefUtils.PREF_KEY_EMERGENCY_NUM, ""));
            item.setEmergencyReasons(sharedPreferences.getString(PrefUtils.PREF_KEY_EMERGENCY_REASONS, ""));
            item.setEmergencyImage(sharedPreferences.getString(PrefUtils.PREF_KEY_EMERGENCY_IMAGE, ""));
            OfflineDbHelper offlineDbHelper = new OfflineDbHelper(getBaseContext());
            offlineDbHelper.addItem(item);
        }
        findViewById(R.id.btnContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ThankYouActivity.this, MainActivity.class));
            }
        });
    }
}
