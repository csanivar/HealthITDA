package com.healthitda.android.ui.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.healthitda.android.MainActivity;
import com.healthitda.android.R;
import com.healthitda.android.utils.NetworkUtil;
import com.healthitda.android.utils.PrefUtils;
import com.healthitda.android.utils.URLs;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.internal.Util;
import com.squareup.picasso.Picasso;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.ByteArrayBuffer;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okio.Buffer;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;


/**
 * Created by mouli on 4/28/15.
 */
public class ChildrenActivity extends BaseActivity {
    private static final String TAG = "ChildrenActivity.class";
    public static final String INTENT_KEY_MANDAL = "intent_key_mandal";
    public static final String INTENT_KEY_PHC = "intent_key_phc";
    public static final String INTENT_KEY_SUB_CENTER = "intent_key_sub_center";
    public static final String INTENT_KEY_VILLAGE = "intent_key_village";
    public static final String INTENT_KEY_NAME = "intent_key_name";
    public static final String INTENT_KEY_SUCCESS_ID = "intent_key_success_id";
    public static final String INTENT_KEY_ONLINE_MODE = "intent_key_online_mode";
    private static final int IMAGE_REQUEST_CODE = 500;
    String mandal;
    String phc;
    String subcenter;
    String village;
    String name;
    int total_children = -1;
    int children_immunised = -1;
    private Uri outputImageUri;
    private Bitmap selectedBitmap;
    private ImageView ivClickedImage;
    private EditText etTotalChildren;
    private EditText etChildrenImmunised;
    private Button btnSubmit;
    private Button btnPicture;
    Uri selectedImageUri;
    ProgressDialog mProgressDialog;
    private boolean storeOffline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_children);
        storeOffline = getIntent().getBooleanExtra(MainActivity.INTENT_KEY_STORE_OFFLINE, false);
        Log.d(TAG, "storeOffline " + storeOffline);
        init();
    }

    public void openImageIntent() {
        File root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                getString(R.string.app_name));
        if (!root.mkdirs()) {
            Log.e(TAG, "Root directory not created");
        }
        String fileName = "img_" + System.currentTimeMillis();
        File imageMainDirectory = null;
        try {
            imageMainDirectory = File.createTempFile(fileName, ".jpg", root);
        } catch (IOException e) {
            Log.e(TAG, "Unable to create image file at " + root.getAbsolutePath());
        }
        outputImageUri = Uri.fromFile(imageMainDirectory);

        List<Intent> cameraIntents = new ArrayList<>();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> cameraList = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo resolveInfo : cameraList) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(packageName, resolveInfo.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputImageUri);
            cameraIntents.add(intent);
        }

        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent chooserIntent = Intent.createChooser(pickIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.
                toArray(new Parcelable[cameraIntents.size()]));
        startActivityForResult(chooserIntent, IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "data " + data);
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_REQUEST_CODE) {
                boolean isCamera = data == null || MediaStore.ACTION_IMAGE_CAPTURE.equals(data.getAction());

                if (isCamera) {
                    selectedImageUri = outputImageUri;
                } else {
                    selectedImageUri = data.getData();
                }

                try {
                    selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImageUri);
                    if(selectedBitmap!=null) {
                        btnPicture.setBackgroundResource(R.drawable.retake_picture);
                        Picasso.with(ChildrenActivity.this).load(selectedImageUri)
                                .resize(400, 600)
                                .into(ivClickedImage);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void init() {
        ivClickedImage = (ImageView) findViewById(R.id.ivClickedImage);
        etTotalChildren = (EditText) findViewById(R.id.etHint1);
        etChildrenImmunised  = (EditText) findViewById(R.id.etHint2);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnPicture = (Button) findViewById(R.id.btnPicture);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras!=null) {
            try {
                mandal = extras.getString(INTENT_KEY_MANDAL);
                phc = extras.getString(INTENT_KEY_PHC);
                subcenter = extras.getString(INTENT_KEY_SUB_CENTER);
                village = extras.getString(INTENT_KEY_VILLAGE);
                name = extras.getString(INTENT_KEY_NAME);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedBitmap == null) {
                    Toast.makeText(ChildrenActivity.this, "Cannot continue without a picture" , Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!etTotalChildren.getText().toString().equals("")) {
                    total_children = Integer.valueOf(etTotalChildren.getText().toString());
                    Log.d(TAG, "total_children " + total_children);
                }
                if (!etChildrenImmunised.getText().toString().equals("")) {
                    children_immunised = Integer.valueOf(etChildrenImmunised.getText().toString());
                    Log.d(TAG, "children_immunised " + children_immunised);
                }
                if (total_children == -1 || children_immunised == -1 ) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ChildrenActivity.this);
                    builder.setIcon(android.R.drawable.ic_dialog_info);
                    builder.setMessage("Do you want to submit partial data and proceed?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            if(storeOffline) {
                                saveDataOffline();
                                return;
                            }
                            if(NetworkUtil.haveNetworkConnection(ChildrenActivity.this)) {
                                onlineMode();
                            } else {
                                saveDataOffline();
                                Toast.makeText(ChildrenActivity.this, getString(R.string.toast_no_internet),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    if(storeOffline) {
                        saveDataOffline();
                        return;
                    }
                    if(NetworkUtil.haveNetworkConnection(ChildrenActivity.this)) {
                        onlineMode();
                    } else {
                        saveDataOffline();
                        Toast.makeText(ChildrenActivity.this, getString(R.string.toast_no_internet),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        btnPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageIntent();
            }
        });
    }

    private void saveDataOffline() {
        writeToSharedPrefs();

        Intent intent = new Intent(ChildrenActivity.this, ANCActivity.class);
        intent.putExtra(NetworkUtil.INTENT_KEY_NO_INTERNET, false);
        intent.putExtra(MainActivity.INTENT_KEY_STORE_OFFLINE, storeOffline);
        startActivity(intent);
    }

    private void onlineMode() {
        writeToSharedPrefs();
        Intent intent = new Intent(ChildrenActivity.this, ANCActivity.class);
        startActivity(intent);
    }

    private void writeToSharedPrefs() {
        SharedPreferences.Editor editor = PrefUtils.getSharedPreferences(ChildrenActivity.this).edit();
        editor.putString(PrefUtils.PREF_KEY_MANDAL, mandal);
        editor.putString(PrefUtils.PREF_KEY_PHC, phc);
        editor.putString(PrefUtils.PREF_KEY_SUB_CENTER, subcenter);
        editor.putString(PrefUtils.PREF_KEY_VILLAGE, village);
        editor.putString(PrefUtils.PREF_KEY_NAME, name);
        editor.putString(PrefUtils.PREF_KEY_CHILDREN_NUM, String.valueOf(children_immunised));
        editor.putString(PrefUtils.PREF_KEY_CHILDREN_TOTAL, String.valueOf(total_children));
        editor.putString(PrefUtils.PREF_KEY_CHILDREN_IMAGE, selectedImageUri.toString());
        editor.commit();
    }

    public class SubmitData extends AsyncTask<Void,Void,String> {

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(ChildrenActivity.this);
            mProgressDialog.setMessage("Uploading Data...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "Submit called");
            if(selectedImageUri==null) {
                Log.d(TAG, "selected image uri is null");
                return "";
            }
            OkHttpClient client = new OkHttpClient();
            final File file = new File(getRealPathFromURI(selectedImageUri, ChildrenActivity.this));
            Bitmap bitmap = Bitmap.createScaledBitmap(selectedBitmap, selectedBitmap.getWidth()/2,
                    selectedBitmap.getHeight()/2, true);
            try {
                FileOutputStream fOut = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                fOut.flush();
                fOut.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addPart(Headers.of("Content-Disposition", "form-data; name=\"mandal\""),
                            RequestBody.create(null, mandal))
                    .addFormDataPart("phc", phc)
                    .addFormDataPart("subcenter", subcenter)
                    .addFormDataPart("village", village)
                    .addFormDataPart("name", name)
                    .addFormDataPart("childern_num", String.valueOf(children_immunised))
                    .addFormDataPart("childern_total", String.valueOf(total_children))
                    .addPart(Headers.of("Content-Disposition", "form-data;name=\"fileToUpload\"; filename=\"" + file.getName() + "\""),
                            RequestBody.create(null, file)
                    )
                    .build();

            Request request = new Request.Builder()
                    .url(URLs.SAVE_DATA_SERVICE)
                    .removeHeader("Content-Type")
                    .post(requestBody)
                    .build();
            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }
            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            mProgressDialog.dismiss();
            Log.d(TAG, "response from on post execute " + response);
            String successId = response.split(":")[1];
            Intent intent = new Intent(ChildrenActivity.this, ANCActivity.class);
            intent.putExtra(INTENT_KEY_SUCCESS_ID, successId);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }

    public static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
}
