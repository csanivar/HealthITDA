package com.healthitda.android.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class PrefUtils {
    private static final String TAG = "PrefUtils.class";
    /**
     * Keys of the shared preferences stored
     */
    public static String PREF_KEY_MANDAL = "mandal";
    public static String PREF_KEY_PHC = "phc";
    public static String PREF_KEY_SUB_CENTER = "sub_center";
    public static String PREF_KEY_VILLAGE = "village";
    public static String PREF_KEY_NAME = "name";
    public static String PREF_KEY_CHILDREN_NUM = "children_num";
    public static String PREF_KEY_CHILDREN_TOTAL = "children_total";
    public static String PREF_KEY_CHILDREN_IMAGE = "fileToUpload";
    public static String PREF_KEY_ANC_NUM = "anc_num";
    public static String PREF_KEY_ANC_NUM_IMMNISED = "anc_num_immunised";
    public static String PREF_KEY_ANC_NAME_MCTS = "anc_name_mcts";
    public static String PREF_KEY_ANC_IMAGE = "anc_pic";
    public static String PREF_KEY_EMERGENCY_NUM = "emergency_num";
    public static String PREF_KEY_EMERGENCY_REASONS = "emergency_reasons";
    public static String PREF_KEY_EMERGENCY_IMAGE = "pic";
    public static String PREF_KEY_IS_THERE_OFFLINE_DATA = "is_there_offline_data";

    /**
     * Constant string for file name to store the SharedPreferences of the
     * application. This is required to get the same SharedPreferences object
     * regardless of the package name of the class
     */
    private static final String FILE_NAME = "default.prefs";

    /**
     * static preference manager that is returned when any class calls
     * for a preference manager
     */
    private static SharedPreferences mSharedPreferences;

    /**
     * returns same SharedPrefs
     * through out the application
     *
     * @param context context
     * @return SharedPreference object
     */
    public static SharedPreferences getSharedPreferences(Context context) {
        if (mSharedPreferences == null) {
            mSharedPreferences = context.getSharedPreferences(FILE_NAME,
                    Context.MODE_PRIVATE);
        }
        return mSharedPreferences;
    }


}
