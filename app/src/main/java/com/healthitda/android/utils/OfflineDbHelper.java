package com.healthitda.android.utils;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import com.healthitda.android.model.Mandal;
import com.healthitda.android.model.OfflineData;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mouli on 5/21/15.
 */
public class OfflineDbHelper extends SQLiteOpenHelper{
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "healthitda";
    private static final String TABLE_ITEMS = "offline_data";

    public static final String KEY_ITEMID = "itemid";
    public static final String KEY_MANDAL = "mandal";
    public static final String KEY_PHC = "phc";
    public static final String KEY_SUB_CENTER = "sub_center";
    public static final String KEY_VILLAGE = "village";
    public static final String KEY_NAME = "name";
    public static final String KEY_CHILDREN_NUM = "children_num";
    public static final String KEY_CHILDREN_TOTAL = "children_total";
    public static final String KEY_CHILDREN_IMAGE = "fileToUpload";
    public static final String KEY_ANC_NUM = "anc_num";
    public static final String KEY_ANC_NUM_IMMNISED = "anc_num_immunised";
    public static final String KEY_ANC_NAME_MCTS = "anc_name_mcts";
    public static final String KEY_ANC_IMAGE = "anc_pic";
    public static final String KEY_EMERGENCY_NUM = "emergency_num";
    public static final String KEY_EMERGENCY_REASONS = "emergency_reasons";
    public static final String KEY_EMERGENCY_IMAGE = "pic";

    public OfflineDbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ITEMS_TABLE = "CREATE TABLE " + TABLE_ITEMS + "("
                + KEY_ITEMID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_MANDAL + " TEXT,"
                + KEY_PHC + " TEXT,"
                + KEY_SUB_CENTER + " TEXT,"
                + KEY_VILLAGE + " TEXT,"
                + KEY_NAME + " TEXT,"
                + KEY_CHILDREN_NUM + " TEXT,"
                + KEY_CHILDREN_TOTAL + " TEXT,"
                + KEY_CHILDREN_IMAGE + " TEXT,"
                + KEY_ANC_NUM + " TEXT,"
                + KEY_ANC_NUM_IMMNISED + " TEXT,"
                + KEY_ANC_NAME_MCTS + " TEXT,"
                + KEY_ANC_IMAGE + " TEXT,"
                + KEY_EMERGENCY_NUM + " TEXT,"
                + KEY_EMERGENCY_REASONS + " TEXT,"
                + KEY_EMERGENCY_IMAGE + " TEXT"
                + ")";
        db.execSQL(CREATE_ITEMS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
        // Create tables again
        onCreate(db);
    }

    public void addItem (OfflineData offlineData) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues vals = new ContentValues();
        vals.put(KEY_MANDAL, offlineData.getMandal());
        vals.put(KEY_PHC, offlineData.getPhc());
        vals.put(KEY_SUB_CENTER, offlineData.getSubCenter());
        vals.put(KEY_VILLAGE, offlineData.getVillage());
        vals.put(KEY_NAME, offlineData.getName());
        vals.put(KEY_CHILDREN_NUM, offlineData.getChildrenNum());
        vals.put(KEY_CHILDREN_TOTAL, offlineData.getChildrenTotal());
        vals.put(KEY_CHILDREN_IMAGE, offlineData.getChildrenImage());
        vals.put(KEY_ANC_NUM, offlineData.getAncNum());
        vals.put(KEY_ANC_NUM_IMMNISED, offlineData.getAncNumImmnised());
        vals.put(KEY_ANC_NAME_MCTS, offlineData.getAncNameMcts());
        vals.put(KEY_ANC_IMAGE, offlineData.getAncImage());
        vals.put(KEY_EMERGENCY_NUM, offlineData.getEmergencyNum());
        vals.put(KEY_EMERGENCY_REASONS, offlineData.getEmergencyReasons());
        vals.put(KEY_EMERGENCY_IMAGE, offlineData.getEmergencyImage());
        db.insert(TABLE_ITEMS, null, vals);
        db.close();
    }

    public void clearItems() {
        SQLiteDatabase db = this.getWritableDatabase();
        this.onUpgrade(db, DB_VERSION, DB_VERSION); //Yes, I know. Genius, ain't I? ;)
        db.close();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public List<OfflineData> getAllItems() throws JSONException {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_ITEMS, null, null, null, null, null, null);

        List<OfflineData> items = new ArrayList<OfflineData>(cursor.getCount());

        cursor.moveToFirst();
        do {
            OfflineData item = new OfflineData();
            item.setItemId(cursor.getInt(cursor.getColumnIndex(KEY_ITEMID)));
            item.setMandal(cursor.getString(cursor.getColumnIndex(KEY_MANDAL)));
            item.setPhc(cursor.getString(cursor.getColumnIndex(KEY_PHC)));
            item.setSubCenter(cursor.getString(cursor.getColumnIndex(KEY_SUB_CENTER)));
            item.setVillage(cursor.getString(cursor.getColumnIndex(KEY_VILLAGE)));
            item.setName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
            item.setChildrenNum(cursor.getString(cursor.getColumnIndex(KEY_CHILDREN_NUM)));
            item.setChildrenTotal(cursor.getString(cursor.getColumnIndex(KEY_CHILDREN_TOTAL)));
            item.setChildrenImage(cursor.getString(cursor.getColumnIndex(KEY_CHILDREN_IMAGE)));
            item.setAncNum(cursor.getString(cursor.getColumnIndex(KEY_ANC_NUM)));
            item.setAncNumImmnised(cursor.getString(cursor.getColumnIndex(KEY_ANC_NUM_IMMNISED)));
            item.setAncNameMcts(cursor.getString(cursor.getColumnIndex(KEY_ANC_NAME_MCTS)));
            item.setAncImage(cursor.getString(cursor.getColumnIndex(KEY_ANC_IMAGE)));
            item.setEmergencyNum(cursor.getString(cursor.getColumnIndex(KEY_EMERGENCY_NUM)));
            item.setEmergencyReasons(cursor.getString(cursor.getColumnIndex(KEY_EMERGENCY_REASONS)));
            item.setEmergencyImage(cursor.getString(cursor.getColumnIndex(KEY_EMERGENCY_IMAGE)));
            items.add(item);
        } while (cursor.moveToNext());

        db.close();

        return items;
    }

    public void deleteItem(OfflineData item) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_ITEMS + " WHERE " + KEY_ITEMID + " = " + item.getItemId());
    }

    public int getCount(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        int count;

        cursor = db.query(TABLE_ITEMS, null, null, null, null, null, null);
        count = cursor.getCount();
        db.close();
        return count;
    }
}
