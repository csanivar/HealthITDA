package com.healthitda.android.model;

/**
 * Created by mouli on 5/21/15.
 */
public class OfflineData {
    private int itemId;
    private  String Mandal;
    private String phc;
    private String subCenter;
    private String village;
    private String name;
    private String childrenNum;
    private String childrenTotal;
    private String childrenImage;
    private String ancNum;
    private String ancNumImmnised;
    private String ancNameMcts;
    private String ancImage;
    private String emergencyNum;
    private String emergencyReasons;
    private String emergencyImage;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getAncImage() {
        return ancImage;
    }

    public void setAncImage(String ancImage) {
        this.ancImage = ancImage;
    }

    public String getMandal() {
        return Mandal;
    }

    public void setMandal(String mandal) {
        Mandal = mandal;
    }

    public String getPhc() {
        return phc;
    }

    public void setPhc(String phc) {
        this.phc = phc;
    }

    public String getSubCenter() {
        return subCenter;
    }

    public void setSubCenter(String subCenter) {
        this.subCenter = subCenter;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChildrenNum() {
        return childrenNum;
    }

    public void setChildrenNum(String childrenNum) {
        this.childrenNum = childrenNum;
    }

    public String getChildrenTotal() {
        return childrenTotal;
    }

    public void setChildrenTotal(String childrenTotal) {
        this.childrenTotal = childrenTotal;
    }

    public String getChildrenImage() {
        return childrenImage;
    }

    public void setChildrenImage(String childrenImage) {
        this.childrenImage = childrenImage;
    }

    public String getAncNum() {
        return ancNum;
    }

    public void setAncNum(String ancNum) {
        this.ancNum = ancNum;
    }

    public String getAncNumImmnised() {
        return ancNumImmnised;
    }

    public void setAncNumImmnised(String ancNumImmnised) {
        this.ancNumImmnised = ancNumImmnised;
    }

    public String getAncNameMcts() {
        return ancNameMcts;
    }

    public void setAncNameMcts(String ancNameMcts) {
        this.ancNameMcts = ancNameMcts;
    }

    public String getEmergencyNum() {
        return emergencyNum;
    }

    public void setEmergencyNum(String emergencyNum) {
        this.emergencyNum = emergencyNum;
    }

    public String getEmergencyReasons() {
        return emergencyReasons;
    }

    public void setEmergencyReasons(String emergencyReasons) {
        this.emergencyReasons = emergencyReasons;
    }

    public String getEmergencyImage() {
        return emergencyImage;
    }

    public void setEmergencyImage(String emergencyImage) {
        this.emergencyImage = emergencyImage;
    }
}
