package com.healthitda.android.utils;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;

import com.healthitda.android.model.Mandal;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mouli on 4/28/15.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "healthitda";
    private static final String TABLE_ITEMS = "items";

    private static final String KEY_NAME = "name";
    private static final String KEY_ITEMID = "itemid";
    private static final String KEY_MANDAL_NAME = "mandal_name";
    private static final String KEY_MANDAL_PHC = "mandal_phc";

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ITEMS_TABLE = "CREATE TABLE " + TABLE_ITEMS + "("
                + KEY_ITEMID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KEY_MANDAL_NAME + " TEXT,"
                + KEY_MANDAL_PHC + " TEXT"
                + ")";
        db.execSQL(CREATE_ITEMS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
        // Create tables again
        onCreate(db);
    }

    public void addItem (Mandal mandal) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues vals = new ContentValues();
        vals.put(KEY_MANDAL_NAME, mandal.getName());
        vals.put(KEY_MANDAL_PHC, mandal.getPhc().toString());
        db.insert(TABLE_ITEMS, null, vals);
        db.close();
    }

    public void clearItems() {
        SQLiteDatabase db = this.getWritableDatabase();
        this.onUpgrade(db, DB_VERSION, DB_VERSION); //Yes, I know. Genius, ain't I? ;)
        db.close();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public List<Mandal> getAllItems() throws JSONException {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_ITEMS, null, null, null, null, null, null);

        List<Mandal> items = new ArrayList<Mandal>(cursor.getCount());

        cursor.moveToFirst();
        do {
            Mandal item = new Mandal();
            item.setName(cursor.getString(cursor.getColumnIndex(KEY_MANDAL_NAME)));
            item.setPhc(new JSONArray(cursor.getString(cursor.getColumnIndex(KEY_MANDAL_PHC))));
            items.add(item);
        } while (cursor.moveToNext());

        db.close();

        return items;
    }

    public int getCount(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor;
        int count;

        cursor = db.query(TABLE_ITEMS, null, null, null, null, null, null);
        count = cursor.getCount();
        db.close();
        return count;
    }
}
