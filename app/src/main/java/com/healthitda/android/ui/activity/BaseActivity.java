package com.healthitda.android.ui.activity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.BaseAdapter;

import com.healthitda.android.service.BaseHttpService;
import com.healthitda.android.utils.EventBus;

/**
 * Created by mouli on 4/28/15.
 */
public class BaseActivity extends Activity implements ServiceConnection {
    private static final String TAG = "BaseActivity.class";
    boolean mBound = false;
    protected BaseHttpService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        Intent serviceIntent = new Intent(this, BaseHttpService.class);
        this.startService(serviceIntent);
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getInstance().unregister(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mBound) {
            Log.d(TAG, "unbinding");
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    public void onServiceConnected(ComponentName className,
                                   IBinder service) {
        // We've bound to LocalService, cast the IBinder and get LocalService instance
        BaseHttpService.HttpGetBinder binder = (BaseHttpService.HttpGetBinder) service;
        mService = binder.getService();
        mBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName arg0) {
        mBound = false;
    }

    private ServiceConnection mConnection = this;

}
