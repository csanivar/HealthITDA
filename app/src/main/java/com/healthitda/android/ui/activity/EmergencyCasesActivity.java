package com.healthitda.android.ui.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.healthitda.android.MainActivity;
import com.healthitda.android.R;
import com.healthitda.android.model.OfflineData;
import com.healthitda.android.utils.NetworkUtil;
import com.healthitda.android.utils.OfflineDbHelper;
import com.healthitda.android.utils.PrefUtils;
import com.healthitda.android.utils.URLs;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import net.steamcrafted.loadtoast.LoadToast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okio.Buffer;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;


/**
 * Created by mouli on 4/28/15.
 */
public class EmergencyCasesActivity extends BaseActivity {
    private static final String TAG = "EmergencyActivity.class";
    private static final int IMAGE_REQUEST_CODE = 500;
    private Uri outputImageUri;
    private Bitmap selectedBitmap;
    private ImageView ivClickedImage;
    private EditText etTotalEmergencies;
    private EditText etReasons;
    private int totalEmergencies = -1;
    private String reasons = "";
    private Button btnSubmit;
    Uri selectedImageUri;
    ProgressDialog mProgressDialog;
    private Button btnPicture;
    private String successId;
    private boolean isNetworkAvailable;
    private boolean storeOffline;
    private LoadToast mLoadToast;
    private String mSuccessId;
    private OfflineData mItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_emergency_cases);
        mLoadToast = new LoadToast(EmergencyCasesActivity.this);
        isNetworkAvailable = getIntent().getBooleanExtra(NetworkUtil.INTENT_KEY_NO_INTERNET, true);
        storeOffline = getIntent().getBooleanExtra(MainActivity.INTENT_KEY_STORE_OFFLINE, false);
        init();
    }

    public void openImageIntent() {
        File root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                getString(R.string.app_name));
        if (!root.mkdirs()) {
            Log.e(TAG, "Root directory not created");
        }
        String fileName = "img_" + System.currentTimeMillis();
        File imageMainDirectory = null;
        try {
            imageMainDirectory = File.createTempFile(fileName, ".jpg", root);
        } catch (IOException e) {
            Log.e(TAG, "Unable to create image file at " + root.getAbsolutePath());
        }
        outputImageUri = Uri.fromFile(imageMainDirectory);

        List<Intent> cameraIntents = new ArrayList<>();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> cameraList = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo resolveInfo : cameraList) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(packageName, resolveInfo.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputImageUri);
            cameraIntents.add(intent);
        }

        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent chooserIntent = Intent.createChooser(pickIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.
                toArray(new Parcelable[cameraIntents.size()]));
        startActivityForResult(chooserIntent, IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "data " + data);
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_REQUEST_CODE) {
                boolean isCamera = data == null || MediaStore.ACTION_IMAGE_CAPTURE.equals(data.getAction());

                if (isCamera) {
                    selectedImageUri = outputImageUri;
                } else {
                    selectedImageUri = data.getData();
                }

                try {
                    selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImageUri);
                    if(selectedBitmap!=null) {
                        Picasso.with(EmergencyCasesActivity.this).load(selectedImageUri)
                                .resize(400, 600)
                                .into(ivClickedImage);
                        btnPicture.setBackgroundResource(R.drawable.retake_picture);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void init() {
        ivClickedImage = (ImageView) findViewById(R.id.ivClickedImage);
        etTotalEmergencies = (EditText) findViewById(R.id.etHint1);
        etReasons = (EditText) findViewById(R.id.etHint2);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnPicture = (Button) findViewById(R.id.btnPicture);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras!=null) {
            try {
                successId = extras.getString(ChildrenActivity.INTENT_KEY_SUCCESS_ID);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!etTotalEmergencies.getText().toString().equals("")) {
                    totalEmergencies = Integer.valueOf(etTotalEmergencies.getText().toString());
                }
                if (!etReasons.getText().toString().equals("")) {
                    reasons = etReasons.getText().toString();
                }
                if (totalEmergencies == -1 || "".equals(reasons) || selectedBitmap==null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EmergencyCasesActivity.this);
                    builder.setIcon(android.R.drawable.ic_dialog_info);
                    builder.setMessage("Do you want to submit partial data and proceed?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            if(storeOffline) {
                                saveDataOffline();
                                return;
                            }
                            if(isNetworkAvailable) {
                                onlineMode();
                            } else {
                                saveDataOffline();
                                Toast.makeText(EmergencyCasesActivity.this, getString(R.string.toast_no_internet),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    if(storeOffline) {
                        saveDataOffline();
                        return;
                    }
                    if(isNetworkAvailable) {
                        onlineMode();
                    } else {
                        saveDataOffline();
                        Toast.makeText(EmergencyCasesActivity.this, getString(R.string.toast_no_internet),
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        btnPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageIntent();
            }
        });
    }

    private void saveDataOffline() {
        writeToSharedPrefs();
        Intent intent = new Intent(EmergencyCasesActivity.this, ThankYouActivity.class);
        intent.putExtra(NetworkUtil.INTENT_KEY_NO_INTERNET, isNetworkAvailable);
        intent.putExtra(MainActivity.INTENT_KEY_STORE_OFFLINE, storeOffline);
        startActivity(intent);
    }

    private void onlineMode() {
        writeToSharedPrefs();
        getItem();
        new SubmitChildrenData().execute();
    }

    private void writeToSharedPrefs() {
        SharedPreferences.Editor editor = PrefUtils.getSharedPreferences(EmergencyCasesActivity.this).edit();
        editor.putString(PrefUtils.PREF_KEY_EMERGENCY_NUM, String.valueOf(totalEmergencies));
        editor.putString(PrefUtils.PREF_KEY_EMERGENCY_REASONS, reasons);
        editor.putString(PrefUtils.PREF_KEY_EMERGENCY_IMAGE, selectedImageUri == null ? "" : selectedImageUri.toString());
        editor.apply();
    }

    private void getItem() {
        SharedPreferences prefs = PrefUtils.getSharedPreferences(EmergencyCasesActivity.this);
        mItem = new OfflineData();
        mItem.setMandal(prefs.getString(PrefUtils.PREF_KEY_MANDAL, ""));
        mItem.setPhc(prefs.getString(PrefUtils.PREF_KEY_PHC, ""));
        mItem.setSubCenter(prefs.getString(PrefUtils.PREF_KEY_SUB_CENTER, ""));
        mItem.setVillage(prefs.getString(PrefUtils.PREF_KEY_VILLAGE, ""));
        mItem.setName(prefs.getString(PrefUtils.PREF_KEY_NAME, ""));
        mItem.setChildrenTotal(prefs.getString(PrefUtils.PREF_KEY_CHILDREN_TOTAL, ""));
        mItem.setChildrenNum(prefs.getString(PrefUtils.PREF_KEY_CHILDREN_NUM, ""));
        mItem.setChildrenImage(prefs.getString(PrefUtils.PREF_KEY_CHILDREN_IMAGE, ""));
        mItem.setAncNum(prefs.getString(PrefUtils.PREF_KEY_ANC_NUM, ""));
        mItem.setAncNumImmnised(prefs.getString(PrefUtils.PREF_KEY_ANC_NUM_IMMNISED, ""));
        mItem.setAncNameMcts(prefs.getString(PrefUtils.PREF_KEY_ANC_NAME_MCTS, ""));
        mItem.setAncImage(prefs.getString(PrefUtils.PREF_KEY_ANC_IMAGE, ""));
        mItem.setEmergencyNum(prefs.getString(PrefUtils.PREF_KEY_EMERGENCY_NUM, ""));
        mItem.setEmergencyReasons(prefs.getString(PrefUtils.PREF_KEY_EMERGENCY_REASONS, ""));
        mItem.setEmergencyImage(prefs.getString(PrefUtils.PREF_KEY_EMERGENCY_IMAGE, ""));
    }

    public class SubmitChildrenData extends AsyncTask<Void,Void,String> {

        @Override
        protected void onPreExecute() {
            mLoadToast.setText("Uploading data...");
            mLoadToast.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            OkHttpClient client = new OkHttpClient();
            final File file = new File(getRealPathFromURI(Uri.parse(mItem.getChildrenImage()), EmergencyCasesActivity.this));

            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addPart(Headers.of("Content-Disposition", "form-data; name=\"mandal\""),
                            RequestBody.create(null, mItem.getMandal()))
                    .addFormDataPart("phc", mItem.getPhc())
                    .addFormDataPart("subcenter", mItem.getSubCenter())
                    .addFormDataPart("village", mItem.getVillage())
                    .addFormDataPart("name", mItem.getName())
                    .addFormDataPart("childern_num", mItem.getChildrenNum())
                    .addFormDataPart("childern_total", mItem.getChildrenTotal())
                    .addPart(Headers.of("Content-Disposition", "form-data;name=\"fileToUpload\"; filename=\"" + file.getName() + "\""),
                            RequestBody.create(null, file)
                    )
                    .build();

            Request request = new Request.Builder()
                    .url(URLs.SAVE_DATA_SERVICE)
                    .removeHeader("Content-Type")
                    .post(requestBody)
                    .build();
            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }
            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            mSuccessId = response.split(":")[1];
            Log.d(TAG, "success id " + mSuccessId);
            new SubmitANCData().execute();
        }
    }

    public class SubmitANCData extends AsyncTask<Void,Void,String> {

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "SubmitANCData called");
            OkHttpClient client = new OkHttpClient();
            final File file = new File(ChildrenActivity.getRealPathFromURI(Uri.parse(mItem.getAncImage()), EmergencyCasesActivity.this));
            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart("id", String.valueOf(mSuccessId))
                    .addFormDataPart("anc_num", mItem.getAncNum())
                    .addFormDataPart("anc_num_immunised", mItem.getAncNumImmnised())
                    .addFormDataPart("anc_name_mcts", mItem.getAncNameMcts())
                    .addPart(Headers.of("Content-Disposition", "form-data;name=\"anc_pic\"; filename=\"" + file.getName() + "\""),
                            RequestBody.create(null, file)
                    )
                    .build();

            Request request = new Request.Builder()
                    .url(URLs.SAVE_DATA_SERVICE_2)
                    .post(requestBody)
                    .build();

            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
                Log.d(TAG, "Response " + response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }

            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            Log.d(TAG, "response from on post execute " + response);
            new SubmitEmergencyData().execute();
        }
    }

    public class SubmitEmergencyData extends AsyncTask<Void,Void,String> {

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "SubmitEmergencyData called");
            OkHttpClient client = new OkHttpClient();
            final File file = new File(getRealPathFromURI(Uri.parse(mItem.getEmergencyImage()), EmergencyCasesActivity.this));
            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart("id", mSuccessId)
                    .addFormDataPart("emergency_num", mItem.getEmergencyNum())
                    .addFormDataPart("reasons", mItem.getEmergencyReasons())
                    .addPart(Headers.of("Content-Disposition", "form-data;name=\"pic\"; filename=\"" + file.getName() + "\""),
                            RequestBody.create(null, file)
                    )
                    .build();

            Request request = new Request.Builder()
                    .url(URLs.SAVE_DATA_SERVICE_3)
                    .post(requestBody)
                    .build();

            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
                Log.d(TAG, "Response " + response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }

            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            mLoadToast.success();
            SharedPreferences.Editor editor = PrefUtils.getSharedPreferences(EmergencyCasesActivity.this).edit();
            editor.putBoolean(PrefUtils.PREF_KEY_IS_THERE_OFFLINE_DATA, false) ;
            editor.apply();
            Log.d(TAG, "response from on post execute emergency " + response);
            OfflineDbHelper helper = new OfflineDbHelper(EmergencyCasesActivity.this);
            helper.deleteItem(mItem);
            Intent intent = new Intent(EmergencyCasesActivity.this, ThankYouActivity.class);
            startActivity(intent);
        }
    }

    public class SubmitData extends AsyncTask<Void,Void,String> {

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(EmergencyCasesActivity.this);
            mProgressDialog.setMessage("Uploading Data...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "GetVillages called");
            if(selectedImageUri==null) {
                Log.d(TAG, "selected image uri is null");
                return "";
            }
            OkHttpClient client = new OkHttpClient();
            final File file = new File(ChildrenActivity.getRealPathFromURI(selectedImageUri, EmergencyCasesActivity.this));
            Bitmap bitmap = Bitmap.createScaledBitmap(selectedBitmap, selectedBitmap.getWidth()/2,
                    selectedBitmap.getHeight()/2, true);
            try {
                FileOutputStream fOut = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                fOut.flush();
                fOut.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart("id", successId)
                    .addFormDataPart("emergency_num", String.valueOf(totalEmergencies))
                    .addFormDataPart("reasons", reasons)
                    .addPart(Headers.of("Content-Disposition", "form-data;name=\"pic\"; filename=\"" + file.getName() + "\""),
                            RequestBody.create(null, file)
                    )
                    .build();

            Request request = new Request.Builder()
                    .url(URLs.SAVE_DATA_SERVICE_3)
                    .post(requestBody)
                    .build();

            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
                Log.d(TAG, "Response " + response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }

            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            mProgressDialog.dismiss();
            Log.d(TAG, "response from on post execute " + response);
            Intent intent = new Intent(EmergencyCasesActivity.this, ThankYouActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }

    public static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }
}
