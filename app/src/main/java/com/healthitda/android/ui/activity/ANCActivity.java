package com.healthitda.android.ui.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Looper;
import android.os.NetworkOnMainThreadException;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.healthitda.android.MainActivity;
import com.healthitda.android.R;
import com.healthitda.android.ui.view.MCTSLinearLayout;
import com.healthitda.android.utils.NetworkUtil;
import com.healthitda.android.utils.PrefUtils;
import com.healthitda.android.utils.URLs;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okio.Buffer;
import okio.BufferedSink;
import okio.Okio;
import okio.Source;


/**
 * Created by mouli on 4/28/15.
 */
public class ANCActivity extends BaseActivity {
    private static final String TAG = "ANCActivity.class";
    private static final int IMAGE_REQUEST_CODE = 500;
    private String successId ;
    private Uri outputImageUri;
    private Bitmap selectedBitmap;
    private ImageView ivClickedImage;
    private EditText etTotalANCs;
    private EditText etANCsImmunised;
    private int totalANCS = -1;
    private int ancsImmunised = -1;
    private Button btnSubmit;
    Uri selectedImageUri;
    ProgressDialog mProgressDialog;
    private MCTSLinearLayout llMCTSValues;
    private Button btnPicture;
    private String anc_mcts_name;
    private boolean isNetworkAvailable;
    private boolean storeOffline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_anc);
        isNetworkAvailable = getIntent().getBooleanExtra(NetworkUtil.INTENT_KEY_NO_INTERNET, true);
        storeOffline = getIntent().getBooleanExtra(MainActivity.INTENT_KEY_STORE_OFFLINE, false);
        init();
    }

    public void openImageIntent() {
        File root = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                getString(R.string.app_name));
        if (!root.mkdirs()) {
            Log.e(TAG, "Root directory not created");
        }
        String fileName = "img_" + System.currentTimeMillis();
        File imageMainDirectory = null;
        try {
            imageMainDirectory = File.createTempFile(fileName, ".jpg", root);
        } catch (IOException e) {
            Log.e(TAG, "Unable to create image file at " + root.getAbsolutePath());
        }
        outputImageUri = Uri.fromFile(imageMainDirectory);

        List<Intent> cameraIntents = new ArrayList<>();
        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = getPackageManager();
        List<ResolveInfo> cameraList = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo resolveInfo : cameraList) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(packageName, resolveInfo.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputImageUri);
            cameraIntents.add(intent);
        }

        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent chooserIntent = Intent.createChooser(pickIntent, "Select source");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.
                toArray(new Parcelable[cameraIntents.size()]));
        startActivityForResult(chooserIntent, IMAGE_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "data " + data);
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_REQUEST_CODE) {
                boolean isCamera = data == null || MediaStore.ACTION_IMAGE_CAPTURE.equals(data.getAction());

                if (isCamera) {
                    selectedImageUri = outputImageUri;
                } else {
                    selectedImageUri = data.getData();
                }

                try {
                    selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImageUri);
                    if(selectedBitmap!=null) {
                        Picasso.with(ANCActivity.this).load(selectedImageUri)
                                .resize(400, 600)
                                .into(ivClickedImage);
                        btnPicture.setBackgroundResource(R.drawable.retake_picture);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void init() {
        ivClickedImage = (ImageView) findViewById(R.id.ivClickedImage);
        etTotalANCs = (EditText) findViewById(R.id.etHint1);
        etANCsImmunised  = (EditText) findViewById(R.id.etHint2);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnPicture = (Button) findViewById(R.id.btnPicture);
        llMCTSValues = (MCTSLinearLayout) findViewById(R.id.llMCTCValues);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if(extras!=null) {
            try {
                successId = extras.getString(ChildrenActivity.INTENT_KEY_SUCCESS_ID);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anc_mcts_name = llMCTSValues.getMCTSName();
                if (!etTotalANCs.getText().toString().equals("")) {
                    totalANCS = Integer.valueOf(etTotalANCs.getText().toString());
                }
                if (!etANCsImmunised.getText().toString().equals("")) {
                    ancsImmunised = Integer.valueOf(etANCsImmunised.getText().toString());
                }
                if (totalANCS == -1 || ancsImmunised == -1 || selectedBitmap==null || anc_mcts_name.equals("")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ANCActivity.this);
                    builder.setIcon(android.R.drawable.ic_dialog_info);
                    builder.setMessage("Do you want to submit partial data and proceed?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            if(storeOffline) {
                                saveDataOffline();
                                return;
                            }
                            if(isNetworkAvailable) {
                                onlineMode();
                            } else {
                                saveDataOffline();
                                Toast.makeText(ANCActivity.this, getString(R.string.toast_no_internet),
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            arg0.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {
                    if(storeOffline) {
                        saveDataOffline();
                        return;
                    }
                    if(isNetworkAvailable) {
                        onlineMode();
                    } else {
                        Toast.makeText(ANCActivity.this, getString(R.string.toast_no_internet),
                                Toast.LENGTH_SHORT).show();
                        saveDataOffline();
                    }
                }
            }
        });
        btnPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageIntent();
            }
        });
    }

    private void saveDataOffline() {
        writeToSharedPrefs();
        Intent intent = new Intent(ANCActivity.this, EmergencyCasesActivity.class);
        intent.putExtra(NetworkUtil.INTENT_KEY_NO_INTERNET, isNetworkAvailable);
        intent.putExtra(MainActivity.INTENT_KEY_STORE_OFFLINE, storeOffline);
        startActivity(intent);
    }

    private void onlineMode() {
        writeToSharedPrefs();
        Intent intent = new Intent(ANCActivity.this, EmergencyCasesActivity.class);
        startActivity(intent);
    }

    private void writeToSharedPrefs() {
        SharedPreferences.Editor editor = PrefUtils.getSharedPreferences(ANCActivity.this).edit();
        editor.putString(PrefUtils.PREF_KEY_ANC_NUM, String.valueOf(totalANCS));
        editor.putString(PrefUtils.PREF_KEY_ANC_NUM_IMMNISED, String.valueOf(totalANCS));
        editor.putString(PrefUtils.PREF_KEY_ANC_NAME_MCTS, anc_mcts_name);
        editor.putString(PrefUtils.PREF_KEY_ANC_IMAGE, selectedImageUri==null ? "" : selectedImageUri.toString());
        editor.apply();
    }

    public class SubmitData extends AsyncTask<Void,Void,String> {

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(ANCActivity.this);
            mProgressDialog.setMessage("Uploading Data...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            Log.d(TAG, "GetVillages called");
            OkHttpClient client = new OkHttpClient();
            if(selectedImageUri==null) {
                Log.d(TAG, "selected image uri is null");
                return "";
            }
            final File file = new File(ChildrenActivity.getRealPathFromURI(selectedImageUri, ANCActivity.this));
            Bitmap bitmap = Bitmap.createScaledBitmap(selectedBitmap, selectedBitmap.getWidth()/2,
                    selectedBitmap.getHeight()/2, true);
            try {
                FileOutputStream fOut = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                fOut.flush();
                fOut.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            RequestBody requestBody = new MultipartBuilder()
                    .type(MultipartBuilder.FORM)
                    .addFormDataPart("id", String.valueOf(successId))
                    .addFormDataPart("anc_num", String.valueOf(totalANCS))
                    .addFormDataPart("anc_num_immunised", String.valueOf(ancsImmunised))
                    .addFormDataPart("anc_name_mcts", anc_mcts_name)
                    .addPart(Headers.of("Content-Disposition", "form-data;name=\"anc_pic\"; filename=\"" + file.getName() + "\""),
                            RequestBody.create(null, file)
                    )
                    .build();

            Request request = new Request.Builder()
                    .url(URLs.SAVE_DATA_SERVICE_2)
                    .post(requestBody)
                    .build();

            String responseBody = null;
            try {
                Response response = client.newCall(request).execute();
                responseBody = response.body().string();
                Log.d(TAG, "Response " + response.body().string());
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(responseBody==null) {
                return "";
            }

            return responseBody;
        }

        @Override
        protected void onPostExecute(String response) {
            mProgressDialog.dismiss();
            Log.d(TAG, "response from on post execute " + response);
            Intent intent = new Intent(ANCActivity.this, EmergencyCasesActivity.class);
            intent.putExtra(ChildrenActivity.INTENT_KEY_SUCCESS_ID, successId);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }
}
