package com.healthitda.android.utils;

/**
 * Created by mouli on 4/28/15.
 */
public class URLs {

    public static final String SUB_CENTERS_SERVICE = "http://acromindia.com/adilabad_health_ws/get_mandal_phc.php";

    public static final String VILLAGES_SERVICE = "http://acromindia.com/adilabad_health_ws/get_village.php";

    public static final String SAVE_DATA_SERVICE = "http://acromindia.com/adilabad_health_ws/save_details.php";

    public static final String SAVE_DATA_SERVICE_2 = "http://acromindia.com/adilabad_health_ws/save_details1.php";

    public static final String SAVE_DATA_SERVICE_3 = "http://acromindia.com/adilabad_health_ws/save_details2.php";
}
